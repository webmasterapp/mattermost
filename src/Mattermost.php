<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace MasterApp\Mattermost;

use JsonException;

/**
 * Class Mattermost
 * @package MasterApp\Mattermost
 */
class Mattermost {

    public string $mattermostHook;

    public string $mattermostChannel;

    public string $mattermostDebugChannel;

    public string $productOwner;

    private bool $isStaging;

    public function setMattermostHook(string $mattermostHook): void {

        $this->mattermostHook = $mattermostHook;
    }

    public function setProductOwner(string $productOwner): void {

        $this->productOwner = $productOwner;
    }

    public function setMattermostChannel(string $mattermostChannel): void {

        $this->mattermostChannel = $mattermostChannel;
    }

    public function setMattermostDebugChannel(string $mattermostDebugChannel): void {

        $this->mattermostDebugChannel = $mattermostDebugChannel;
    }

    public function setIsStaging(bool $isStaging): void {

        $this->isStaging = $isStaging;
    }

    /**
     * @param string $message
     * @param bool $isDebug
     * @throws MattermostException
     */
    public function sendMessage(string $message, bool $isDebug = false): void {

        // Init
        $updatedMessage = $this->isStaging ? "** STAGING **\n" . $message : $message;
        $ch = curl_init($this->mattermostHook);
        if ($ch === false) {
            throw new MattermostException('Can not init cURL');
        }
        $channel = $isDebug ? $this->mattermostDebugChannel : $this->mattermostChannel;
        $data = ['text' => $updatedMessage, 'channel' => $channel, 'username' => $this->productOwner];

        // Encode
        try {
            $data = json_encode($data, JSON_THROW_ON_ERROR);
        } catch (JsonException) {
            throw new MattermostException('Can not encode data');
        }

        // Send
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'cache-control: no-cache']);
        curl_exec($ch);
        curl_close($ch);
    }
}