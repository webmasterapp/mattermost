<?php

declare(strict_types=1);

namespace MasterApp\Mattermost\DI;

use MasterApp\Mattermost\Mattermost;
use Nette\DI\CompilerExtension;

/**
 * Class GraphQLFrontApiExtension
 * @package MasterApp\Mattermost\DI
 */
class MattermostExtension extends CompilerExtension {

    public function loadConfiguration(): void {

        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('mattermost'))
            ->setType(Mattermost::class)->addSetup('setMattermostHook', [$config['hook']])
            ->addSetup('setMattermostChannel', [$config['channel']])
            ->addSetup('setIsStaging', [$config['staging'] ?? false])
            ->addSetup('setMattermostDebugChannel', [$config['debugChannel'] ?? $config['channel']])
            ->addSetup('setProductOwner', [$config['owner'] ?? 'Mattermost LIB']);
    }
}
